const mongoose = require("mongoose");

const mongoSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
    },
    email:{
        type:String,
        required:true,
        unique:true
    },
    password:{
        type:String,
        required:true
    },
    cpassword:{
        type:String,
        required:true
    }

})
const includ = mongoose.model('product',mongoSchema);

module.exports = includ;

