// import express from "express";
const express = require("express");
const path = require("path");
const includ = require("./mod/model");
const hbs = require("hbs");
// const { brotliDecompressSync } = require("zlib");
// const { render } = require("express/lib/response");
// const { model } = require("mongoose");
require("./db/conn");

const app = express();
const port = process.env.PORT || 7000;

// console.log(path.join(__dirname,"..models/model"));
 const static = path.join(__dirname,"../public");
 const temp_path = path.join(__dirname,"../templete/views");
 const partial_path = path.join(__dirname,"../templete/partials");

app.use(express.static(static)); 
app.set("view engine",'hbs');
app.set("views",temp_path);
hbs.registerPartials(partial_path);

app.use(express.json());
app.use(express.urlencoded({extended:false}));

app.get("/", (req,res) => {
    res.render("login");
});
// app.get("/login", (req,res) => {
//     res.render("login");
// });
// app.get("/insert", (req,res) => {
//     res.render("insert");
// });
app.post("/login", async (req,res) => {
    try{
        const password = req.body.password;
        const cpassword =req.body.cpassword;
        if(password === cpassword){
          const registerDoc = new includ({
            name:req.body.name,
            email:req.body.email,
            password:req.body.password,
            cpassword:req.body.cpassword
          })
            const register = await registerDoc.save();
            res.render("./login");
        }else{
            res.send("Username password are not match");
        }
    
    }catch(e){
        res.status(400).send(e);
    }
});
// const user = new dataim({name : req.body.name,email : req.body.email, password : req.body.password});
//     user.save().then(()=>{
//         res.status(201).send(user);
//     }).catch((e)=>{
//        res.status(401).send(e);
//     });
// });

app.listen(port, () => {
    console.log(`listen port no is : ${port}`);
})